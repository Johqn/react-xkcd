
export const LOAD_XKCD = 'XKCD::LOAD_XKCD'

export const loadXkcd = xkcdId => async dispatch => {
    const url = 'https://xkcd.com/' 
            + (xkcdId ? (xkcdId + '/') : '')
            + 'info.0.json'
    const proxy = 'https://cors-anywhere.herokuapp.com/'
    try {
        const res = await fetch(proxy + url)
        const data = await res.json()
        dispatch({
            type: LOAD_XKCD,
            payload: data
        })
    }
    catch (err) {
        console.error('Error loading XKCD.')
    }
}

export const xkcdReducer = (state = null, action) => {

    switch (action.type) {
        case LOAD_XKCD:
            return action.payload
        default:
            return state
    }
}
