import React, { Component } from 'react'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import Xkcd from './components/Xkcd'
import Input from './components/Input';
import { xkcdReducer, loadXkcd } from './flux/xkcd'

const initialState = {
    xkcd: {}
}

const mainReducer = combineReducers({
    xkcd: xkcdReducer
})

const store = createStore(
    mainReducer,
    initialState,
    applyMiddleware(thunk)
)

class App extends Component {
    componentDidMount() {
        loadXkcd()(store.dispatch)
    }

    render() {
        return (
            <Provider store={store}>
                <Input />
                <Xkcd />
            </Provider>
        )
    }
}

export default App
