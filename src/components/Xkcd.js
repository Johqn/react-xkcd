import React from 'react'
import { connect } from 'react-redux'
import leftPad from 'left-pad'

const Xkcd = ({
    xkcd: {
        title,
        img,
        alt,
        transcript,
        num,
        year,
        month,
        day,
    } = {},
} = {}) => {
    return (
        <div style={{
            textAlign: 'center'
        }}>
            <h3>{title}</h3>
            <h6>{num} : {leftPad(day, 2, '0')}-{leftPad(month, 2, '0')}-{year}</h6>
            <img
                title={alt}
                src={img}
                alt={transcript}
            />
        </div>
    )
}

const mapStateToProps = ({ xkcd }) => ({
    xkcd
})

export default connect(
    mapStateToProps
)(Xkcd)
