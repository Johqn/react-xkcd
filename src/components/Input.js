import React, { useState } from 'react'
import { loadXkcd } from '../flux/xkcd'
import { connect } from 'react-redux';

const Input = ({
    loadXkcd,
    currentXkcdNum,
}) => {

    const [value, setValue] = useState('')

    const handleChange = event => {
        setValue(event.target.value)
    }

    const handleKeyDown = event => {
        if (event.key === 'Enter') {
            loadXkcd(value)
            setValue('')
        }
    }

    return (
        <div>
            <input
                type='text'
                placeholder='Choose a XKCD id ...'
                value={value}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
            />
            <button onClick={() => loadXkcd(currentXkcdNum - 1)}>previous</button>
            <button onClick={() => loadXkcd(currentXkcdNum + 1)}>next</button>
        </div>
    )
}

const mapDispatchToProps = {
    loadXkcd,
}

export default connect(
    ({ xkcd:{ num }}) => ({ currentXkcdNum: num }),
    mapDispatchToProps
)(Input)
